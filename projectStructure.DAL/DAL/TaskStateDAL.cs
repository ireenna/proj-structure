﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projectStructure.DAL
{
    public enum TaskStateDAL
    {
        ToDo,
        InProgress,
        Done,
        Canceled
    }
}
